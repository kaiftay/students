package com.epam.students;

import java.sql.SQLOutput;
import java.util.Random;

public class StudentApplication {
    private static Random random = new Random();

    public static void main(String[] args) {

        StudentGroup studentGroup1 = new StudentGroup(5);
        StudentGroup studentGroup2 = new StudentGroup(5);

        for (int i = 0; i < 5; i++) {
            studentGroup1.addStudent(new Student("Student " + i, getRandomStudentProgress()));
            studentGroup2.addStudent(new Student("Student " + i, getRandomStudentProgress()));

        }
        System.out.println(studentGroup1);
        System.out.println(studentGroup2);
        System.out.println("Средний балл 1 группы: " + ProgressServise.averageGroupMark(studentGroup1));
        System.out.println("Средний балл 2 группы: " + ProgressServise.averageGroupMark(studentGroup2));
        System.out.println("Количество отличников 1 группы: " + ProgressServise.countAGradeStudent(studentGroup1));
        System.out.println("Количество отличноков 2 группы: " + ProgressServise.countAGradeStudent(studentGroup2));
        System.out.println("Количество студентов с 2, 1 группы:: " + ProgressServise.countDGradeStudent(studentGroup1));
        System.out.println("СКоличество студентов с 2, 2 группы:: " + ProgressServise.countDGradeStudent(studentGroup2));
        for (int i = 0; i < 5; i++) {
            Student student = studentGroup1.getStudents()[i];
            System.out.println("Средний балл студента " + student.getName() + " Из группы 1 :" + ProgressServise.averageStudentMark(student));
        }
        for (int i = 0; i < 5; i++) {
            Student student = studentGroup2.getStudents()[i];
            System.out.println("Средний балл студента " + student.getName() + " Из группы 2 :" + ProgressServise.averageStudentMark(student));
        }
    }

    private static StudentProgress getRandomStudentProgress() {
        StudentProgress studentProgress = new StudentProgress(5);
        int[] marks = studentProgress.getMarks();
        for (int i = 0; i < marks.length; i++) {
            studentProgress.addMark(2 + random.nextInt(4));

        }
        return studentProgress;
    }


}
