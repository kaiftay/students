package com.epam.students;

public class ProgressServise {
    public static double averageStudentMark(Student student) {
        StudentProgress studentProgress = student.getStudentProgress();
        int[] marks = studentProgress.getMarks();
        double averageMark = 0;
        for (int mark : marks) {
            averageMark += mark;
        }
        averageMark /= studentProgress.getCountMarks();
        return averageMark;
    }

    public static double averageGroupMark(StudentGroup studentGroup) {
        double averageMark = 0;
        Student[] students = studentGroup.getStudents();
        for (int i = 0; i < studentGroup.getStudentsCount(); i++) {
            averageMark += averageStudentMark(students[i]);
        }
        averageMark /= studentGroup.getStudentsCount();
        return averageMark;
    }

    public static int countAGradeStudent(StudentGroup studentGroup) {
        Student[] students = studentGroup.getStudents();
        int count = 0;
        for (int i = 0; i < studentGroup.getStudentsCount(); i++) {
            if (isAGradeStudent(students[i]))
                count++;
        }
        return count;
    }

    public static int countDGradeStudent(StudentGroup studentGroup) {
        Student[] students = studentGroup.getStudents();
        int count = 0;
        for (int i = 0; i < studentGroup.getStudentsCount(); i++) {
            if (hasDGrade(students[i]))
                count++;
        }
        return count;
    }

    public static boolean isAGradeStudent(Student student) {
        StudentProgress studentProgress = student.getStudentProgress();
        int[] marks = studentProgress.getMarks();
        for (int i = 0; i < studentProgress.getCountMarks(); i++) {
            if (marks[i] != 5)
                return false;
        }
        return true;
    }

    public static boolean hasDGrade(Student student) {
        StudentProgress studentProgress = student.getStudentProgress();
        int[] marks = studentProgress.getMarks();
        for (int i = 0; i < studentProgress.getCountMarks(); i++) {
            if (marks[i] == 2)
                return true;
        }
        return false;

    }
}