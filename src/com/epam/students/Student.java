package com.epam.students;

public class Student {
    private String name;
    private StudentProgress studentProgress;

    public Student(String name, StudentProgress studentProgress) {
        this.name = name;
        this.studentProgress = studentProgress;
    }

    public StudentProgress getStudentProgress() {
        return studentProgress;
    }

    public void setStudentProgress(StudentProgress studentProgress) {
        this.studentProgress = studentProgress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", studentProgress=" + studentProgress +
                '}';
    }
}
