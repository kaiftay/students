package com.epam.students;

import java.util.Arrays;

public class StudentGroup {
    private Student[] students;

    private int index;

    public StudentGroup(int size) {
        this.students = new Student[size];
    }

    public void addStudent(Student student) {
        if (index == students.length)
            return;
        students[index++] = student;
    }

    public Student[] getStudents() {
        return students;
    }

    public int getStudentsCount() {
        return index;
    }

    @Override
    public String toString() {
        return "StudentGroup{" +
                "students=" + Arrays.toString(students) +
                '}';
    }
}
