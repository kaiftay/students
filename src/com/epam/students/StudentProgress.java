package com.epam.students;

import java.util.Arrays;

public class StudentProgress {
    private int[] marks;
    private int index;

    public StudentProgress(int size) {
        marks = new int[size];
    }

    public void addMark(int mark) {
        if (mark < 2 || mark > 5)
            return;
        if (index >= marks.length)
            return;
        marks[index++] = mark;
    }

    public int getCountMarks() {
        return index;
    }

    public int[] getMarks() {
        return marks;
    }

    @Override
    public String toString() {
        return "StudentProgress{" +
                "marks=" + Arrays.toString(marks) +
                '}';
    }
}
